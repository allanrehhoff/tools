<?php
/**
* Autoloader class for phpunit test cases
* Might be broken for some classes, but it works for now
* @todo Rewrite this crap class later on.
*/
class AutoLoader {
    static public $classNames = array();
    
    public static function registerDirectory($dirName) {
        $di = new DirectoryIterator($dirName);
        foreach ($di as $file) {
            if ($file->isDir() && !$file->isLink() && !$file->isDot()) {
                // recurse into directories other than a few special ones
                self::registerDirectory($file->getPathname());
            } elseif (substr($file->getFilename(), -4) === '.php') {
                // save the class name / path of a .php file found
                $className = str_replace(".class.php", '', $file->getFilename()); //substr($file->getFilename(), 0, -4);
                self::registerClass($className, $file->getPath().'/'.$className.".class.php");
            }
        }
    }
 
    public static function registerClass($className, $fileName) {
        self::$classNames[$className] = $fileName;
    }
 
    public static function loadClass($className) {
        if (isset(self::$classNames[$className])) {
            require_once self::$classNames[$className];
        }
    }
}

spl_autoload_register(array("AutoLoader", "loadClass"));
// Edit this path to your phptools location
AutoLoader::registerDirectory("/usr/home/rehhoo/Tools/phptools/");
