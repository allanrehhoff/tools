<?php
// Simple example on how to authorize against MailChimps API
// $req = new WebRequest("https://<dc>.api.mailchimp.com/3.0/lists");
// $req->authorize("apikey:<your-api-key>");
// $req->call(WebRequest::GET);

// Simple way to get the content of a webpage
// print (new WebRequest("http://rehhoff.me"))->get()->getResponse();
// use "->setHeader($headernane, $headervalue);" before ->get() to provide additional headers

// Or check the headers return by a server
// $req = (new WebRequest("http://rehhoff.me"))->head()->getResponseHeaders();
// Why not set a timeout now that we are at it.
// $req = (new WebRequest("http://rehhoff.me"))->head(10)->getResponseHeaders();

// You should also be able to use an unsupported HTTP Request.
// $req = (new WebRequest("http://rehhoff.me/help-for-example"))->call("OPTIONS")->getResponse();

// Example on how to add a new subscriber to your mailchimp list.
// $subscriber = json_encode( array(
// 	"email_address" => $email,
// 	"status" => "subscribed",
// 	"merge_fields" => array(
// 		"FNAME" => $firstname,
// 		"LNAME" => $lastname
// 	) ) );
// 
// $req = new WebRequest("https://<dc>.api.mailchimp.com/3.0/lists/<list-id>/members");
// $req->authorize("apikey:<your-api-key>");
// $req->post($subscriber, 15); // Second paramter set's timeout.

// At some point you might also want to debug the request
// print (new WebRequest("http://rehhoff.me"))->verbose()->head()->getResponseHeaders();

// $request->getInfo("http_code"); Will return an integer representing the HTTP status of the request performed.
// $request->getResponseHeader("http_code"); Will return the header string containing the status code.
//
// You can also print the object to see response formatted.
// print (new WebRequest())->setOpt(CURLOPT_URL, "http://rehhoff.me")->verbose()->head();

class WebRequestTest extends PHPUnit_Framework_TestCase {
	/**
	* Tests GET request works as expected
	* @author Allan Rehhoff
	*/	
	public function testGetRequest() {
		$req = (new WebRequest("https://httpbin.org/get"))->get();
		$this->assertEquals(200, $req->getInfo("http_code"));
	}

	/**
	* Tests POST request works as expected
	* @author Allan Rehhoff
	*/
	public function testPostRequest() {
		$req = (new WebRequest("https://httpbin.org/post"))->post();
		$this->assertEquals(200, $req->getInfo("http_code"));
	}

	/**
	* Tests PATCH request works as expected
	* @author Allan Rehhoff
	*/
	public function testPatchRequest() {
		$req = (new WebRequest("https://httpbin.org/patch"))->patch();
		$this->assertEquals(200, $req->getInfo("http_code"));
	}

	/**
	* Tests PUT request works as expected
	* @author Allan Rehhoff
	*/
	public function testPutRequest() {
		$req = (new WebRequest("https://httpbin.org/put"))->put();
		$this->assertEquals(200, $req->getInfo("http_code"));
	}

	/**
	* Now test that we can actually put stuff.
	*/
	public function testPutData() {
		$req = new WebRequest("https://httpbin.org/put");
		$req->put(http_build_query(["foo" => "bar"]));
		$response = json_decode($req->getResponse());

		$this->assertTrue(isset($response->form->foo));
	}

	/**
	* Tests DELETE request works as expected
	* @author Allan Rehhoff
	*/
	public function testDeleteRequest() {
		$req = (new WebRequest("https://httpbin.org/delete"))->delete();
		$this->assertEquals(200, $req->getInfo("http_code"));
	}

	/**
	* Test GET request parameters are send as expected
	* @author Allan Rehhoff
	*/
	public function testGetRequestParams() {
		$req = (new WebRequest("https://httpbin.org/get?john=doe"))->get();
		$response = json_decode($req->getResponse());
		$this->assertNotEmpty($response->args);
		$this->assertEquals("doe", $response->args->john);

		$params = ["meal" => "pizza", "toppings" => ["cheese", "ham", "pineapple", "bacon"]];
		$req2 = (new WebRequest("https://httpbin.org/get"))->get($params);
		$response2 = json_decode($req2->getResponse(), true);
		$this->assertNotEmpty($response2["args"]);
		
		/*
		* I check the keys this way because the returned keys are in this format:
		* Array (
     	* 	'meal' => 'pizza'
		* 	'toppings[0]' => 'cheese'
		* 	'toppings[1]' => 'ham'
		* 	'toppings[2]' => 'pineapple'
		* 	'toppings[3]' => 'bacon'
		* 	'toppings' => Array (...)
 		* )
 		* 
		* When what I really wanted was this:
		* Array (
		*	    [meal] => pizza
		*	    [toppings] => Array (
		*	    	[0] => cheese
		*	    	[1] => ham
		*	    	[2] => pineapple
		*	    	[3] => bacon
		*	    )
		*	)
		*/
		$args = $response2["args"];
		foreach($params["toppings"] as $key => $value) {
			$key2check = "toppings[".$key.']';
			if(!isset($args[$key2check]) || $args[$key2check] != $value) {
				$this->fail("A response key/value was not properly returned.");
			}
		}
	}

	/**
	* Test we are able to send a header
	* @author Allan Rehhoff
	*/
	public function testHeaders() {
		$ourHeaders = [
			"X-Firstname" => "John",
			"X-Lastname" => "Doe",
		];

		$req = new WebRequest("https://httpbin.org/headers");
		foreach($ourHeaders as $key => $value) {
			$req->setHeader($key, $value);
		}

		$req->get();
		$response = json_decode($req->getResponse(), true);

		$this->assertNotEmpty($response["headers"]);

		$responseHeadersInCommonWtihOurHeaders = array_intersect($response["headers"], $ourHeaders);

		// This should assert that we got all our headers back.
		$this->assertEquals($ourHeaders, $responseHeadersInCommonWtihOurHeaders);
	}

	/**
	* Test cookies can be set
	* @author Allan Rehhoff
	*/
	public function testSetCookie() {
		$cookies = [
			"fruit" => "apple",
			"vegetable" => "cabbage"
		];
		$req = new WebRequest("https://httpbin.org/cookies");
		foreach($cookies as $name => $value) {
			$req->setCookie($name, $value);
		}
		$response = $req->get()->getResponse();
		$response = json_decode($response, true);
		
		$this->assertEquals($cookies, $response["cookies"]);
	}

	/**
	* Test recieving a single cookie works.
	* @author Allan Rehhoff
	*/
	public function testSingleCookie() {
		$cookie = ["john" => "doe"];
		$req = new WebRequest("https://httpbin.org/cookies/set");
		$response = $req->get($cookie);

		$singleCookie = $req->getCookie("john");
		$this->assertNotEmpty($singleCookie);
	}

	/**
	* Test a remote server can set multiple cookies.
	* @author Allan Rehhoff
	*/
	public function testRecieveMultipleCookies() {
		$cookies = [
			"meal" => "pizza",
			"topping" => "cheese"
		];

		$req = new WebRequest("https://httpbin.org/cookies/set");
		$cookiesRecieved = $req->get($cookies)->getCookies();
		
		// Assert cookie integretity
		$this->assertNotEmpty($cookiesRecieved);
		foreach($cookies as $cookieName => $cookieValue) {
			$this->assertArrayHasKey($cookieName, $cookiesRecieved);
		}
	}

	/**
	* Test user agent spoofing
	* @author Allan Rehhoff
	*/
	public function testUserAgentSpoofing() {
		$agent = "Mozilla/5.0 (Windows; U; Windows NT 6.1; rv:2.2) Gecko/20110201";

		$req = new WebRequest("https://httpbin.org/user-agent");
		$req->setOption(CURLOPT_USERAGENT, $agent);
		$response = $req->get()->getResponse();
		$response = json_decode($response, true);

		$this->assertEquals($agent, $response["user-agent"]);
	}

	/**
	* Test some poor developer wont end up in a black hole somewhere.
	* @author Allan Rehhoff
	*/
	public function testMaxRedirs() {
		try {
			$numRedirs = 10;
			$req = new WebRequest("https://httpbin.org/redirect/".$numRedirs);
			$req->get()->getResponse();
		} catch(Exception $e) {
			$this->assertEquals(CURLE_TOO_MANY_REDIRECTS, $e->getCode());
			return;
		}
		$this->fail("Expected exception [Maximum (5) redirects followed] was not thrown.");
	}

	/**
	* Oh god, i'm not done yet, let's find out if we're able to do a basic HTTP authentication
	* @author Allan Rehhoff
	*/
	public function testBasicAuth() {
		$u = "john";
		$p = "doe";

		$req = new WebRequest("https://httpbin.org/hidden-basic-auth/".$u.'/'.$p);
		$req->authorize($u.':'.$p, CURLAUTH_BASIC)->get();
		$this->assertEquals(200, $req->getInfo("http_code"));

		$response = json_decode($req->getResponse());
		$this->assertTrue($response->authenticated);
	}

	/**
	* Most developers most likely wont need digest access authentication, but i'm going to test it anyway.
	* Even though I know that it will be a pain in the a*s.
	* @author Allan Rehhoff
	* @todo Rewrite (or debug) this piece of garbage.
	*/
	public function testDigestAuth() {
		// Seriously. f*ck this thing, i've spent numourus days already debugging this, and have been hitting a brick wall non-stop
		$this->assertTrue(true);
		return true;

		$username = "john";
		$password = "doe";
		$requestUrl = "https://httpbin.org/digest-auth/auth/".$username.'/'.$password;
		$req = (new WebRequest($requestUrl))->get();

		if($req->getResponseHeader("WWW-Authenticate") !== null) {
			$authHeader = str_replace("Digest ", '', $req->getResponseHeader("WWW-Authenticate"));
			$authHeaderParts = explode(',', $authHeader);
			$authParts = [];

			foreach($authHeaderParts as $pair) {
				$pairValues = explode('=', $pair);
				$authParts[trim($pairValues[0])] = trim($pairValues[1], '" ');
			}

			$realm = (isset($authParts['realm'])) ? $authParts['realm'] : "";
			$nonce = (isset($authParts['nonce'])) ? $authParts['nonce'] : "";
			$opaque = (isset($authParts['opaque'])) ? $authParts['opaque'] : "";

			$authenticate1 = md5($username.":".$realm.":".$password);
			$authenticate2 = md5("GET".":".$requestUrl);

			$authResponse = md5($authenticate1.":".$nonce.":".$authenticate2);

			$authStr = sprintf('Digest username="%s", realm="%s", nonce="%s", opaque="%s", uri="%s", response="%s"', $username, $realm, $nonce, $opaque, $requestUrl, $authResponse);

			$req2 = new WebRequest($requestUrl);
			$req2->authorize($authStr, CURLAUTH_DIGEST)->get()->getResponseHeaders();
		}

		$this->assertEquals(200, $req->getInfo("http_code"));
		$response = json_decode($req->getResponse());
		$this->assertTrue($response->authenticated);
	}

	/**
	* Test posting a file.
	* @author Allan Rehhoff
	*/
	public function testPostFileRequest() {
		// Create a temporary file, for the purpose of this test.
		// Could be any file path
		$time = time();
		$tmpfile =  tempnam("/tmp", $time);
		$tmpfileHandle = fopen($tmpfile, "r+");
		fwrite($tmpfileHandle, $time);

		// As of PHP 5.5 CURLFile objects should be used instead for POSTing files.
		$cfile = new CURLFile($tmpfile, mime_content_type($tmpfile),'tmpfile.txt');
		$data = array('tmpfile' => $cfile);

		// Let's now do the more fancy part
		$req = new WebRequest("https://httpbin.org/post");
		$req->setHeader("Content-Type", "multipart/form-data");
		$res = $req->post($data)->getResponse();
		
		// Time to validate the data we got.
		$res = json_decode($res);
		$this->assertNotEmpty($res->files);
		$this->assertEquals($time, $res->files->tmpfile);
	}

	/**
	* Test putting a file.
	* In a real world scenario one should consider POSTing a file instead.
	* I'm only testing this because i'm corious about if this approach actually works.
	* @see testPostFileRequest
	* @todo Finish this test.
	* @author Allan Rehhoff
	*/
	public function testPutFileRequest() {
		// I hate having to do it this way, but appearently tempnam(); and tmpfile(); hangs the cURL request.
		$filepath = __FILE__;
		$tmpfileHandle = fopen($filepath, 'r');

		$req = new WebRequest("https://httpbin.org/put");
		$req->setOption(CURLOPT_INFILE, $tmpfileHandle);
		$req->setOption(CURLOPT_INFILESIZE, filesize($filepath));
		$req->setOption(CURLOPT_PUT, true);
		$res = $req->call(false, false, 60)->getResponse();
		$res = json_decode($res);

		$this->assertNotEmpty($res->data);
	}

}
?>