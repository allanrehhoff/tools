<?php
/*
* @author Allan Thue Rehhoff
* @version 3.0
* @description
* Simple usage example:
  $sub = new TheChimp3("<apikey>");
  $sub->post("/lists/<list-id>/members", array(
  	"email_address" => "allan.rehhoff@gmail.com",
  	"status" => "subscribed",
  	"merge_fields" => array(
  		"FNAME" => "Allan",
  		"LNAME" => "Rehhoff"
  	)
  ));

  $response =  $sub->getResponse();
}
* TODO: have this wrap around WebRequest instead of raw cURL.
*/
class TheChimp3 {
	private $headers, $ch, $endpoint;
	public $response, $httpcode;

	public function __construct($apikey = null) {
		if($apikey == null) {
			throw new Exception("An apikey is required for requests.");
		}

		$keyParts = explode('-', $apikey);

		$this->endpoint = "https://".$keyParts[1].".api.mailchimp.com/3.0";;
		$this->response = null;

		$this->setHeader("Authorization: Basic ".$apikey);
		$this->setHeader("Content-Type: application/json");
	}

	public function setHeader($headerString) {
		$this->headers[] = $headerString;
	}

	public function getResponse() {
		if($this->response == null) {
			throw new Exception("No response data to show.");
		}

		return json_decode($this->response);
	}

	public function getHttpcode() {
		return $this->httpcode;
	}

	public function execute() {
		$this->response = curl_exec($this->ch);
		$this->httpcode = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
		curl_close($this->ch);
	}

	private function makeReady($uri) {
		if(substr($uri, 0, 1) != '/') {
			throw new Exception("Request uri must start with a slash.");
		}

		if(is_resource($this->ch)) {
			curl_close($this->ch);
		}

		$this->ch = curl_init();
		curl_setopt($this->ch, CURLOPT_USERAGENT, 'PHP-MCAPI/3.0');
		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($this->ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($this->ch, CURLOPT_HTTPHEADER, $this->headers);
		curl_setopt($this->ch, CURLOPT_URL, $this->endpoint.$uri);

		return $this;
	}

	public function post($uri, $data) {
		$this->makeReady($uri);

		curl_setopt($this->ch, CURLOPT_POST, true);
		curl_setopt($this->ch, CURLOPT_POSTFIELDS, json_encode($data));

		$this->execute();
	}

	public function put($uri, $data) {
		$this->makeReady($uri);

		curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($this->ch, CURLOPT_POSTFIELDS, json_encode($data));

		$this->execute();
	}

	public function get($uri, $data) {
		$this->makeReady($uri);

		throw new Exception("This method has yet to be implemented. :) ");

		$this->execute();
	}

	public function delete($uri, $data) {
		$this->makeReady($uri);

		throw new Exception("This method has yet to be implemented. :) ");

		$this->execute();
	}

	public function patch($uri, $data) {
		$this->makeReady($uri);

		throw new Exception("This method has yet to be implemented. :) ");

		$this->execute();
	}
}
