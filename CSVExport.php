<?php
class CSVExport {
	protected $data = [];
	protected $headerFields = [];

	private $delimiter;

	const TAB = "\t";
	const CRLF = "\r\n";
	
	public function __construct($data, $useKeysAsHeaders = true, $delimiter = self::TAB) {
		$this->data = $data;
		
		$this->delimiter = $delimiter;

		if($useKeysAsHeaders) $this->useKeysAsHeaders();
	}
	
	/**
	* Use the keys of the first row as header keys
	* @return object $this
	*/
	public function useKeysAsHeaders() {
		if(count($this->data) > 0) {
			$this->headerFields = array_keys($this->data[0]);
		}

		return $this;
	}

	/**
	* Use the first row as header instead of keys.
	*/
	public function useFirstRowAsHeader() {
		$this->headerFields = $this->data[0];
		unset($this->data[0]);

		return $this;
	}

	/**
	* Get CSV data from constructors data array
	* @return string - The CSV data.
	*/
	public function getCSV() {
		$lines = [];
		
		if(count($this->headerFields) > 0) {
			$lines[] = implode($this->delimiter, $this->headerFields);
		}

		foreach($this->data as $lineData) {
			$lines[] = implode($this->delimiter, $lineData);
		}

		$csv = implode(self::CRLF, $lines);

		return chr(0xFF).chr(0xFE).mb_convert_encoding($csv, 'UTF-16LE', 'UTF-8');
	}

	/**
	* Sends the CSV data as a file to the browser
	* @param string $filename - Name of the file the browser recieves
	* @return object $this;
	*/
	public function sendFile($filename) {
		header("Content-Type: text/csv; charset=UTF-16LE");
		header("Content-Disposition: attachment; filename=" . basename($filename));
		header("Pragma: no-cache");
		header("Expires: 0");

		print $this->getCSV();

		return $this;
	}

	/**
	* Save the CSV data to a file on the filesyste,
	* @param string $location - Full system path where the file should be saved to
	* @return int - Number of bytes written
	*/
	public function saveFile($location) {
		$data = $this->getCSV();

		$fp = fopen($location, "w+");

		for ($written = 0; $written < mb_strlen($data); $written += $fwrite) {
			$fwrite = fwrite($fp, mb_substr($data, $written));

			if ($fwrite === false) {
				return $written;
			}
		}

		return $written;
	}
}