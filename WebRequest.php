<?php
/**
* Provides a (relatively?) easy way of performing RESTful web requests.
* There are usage examples available in the attached WebRequestTest 
* (you should be able to locate that in the reposity using the link in this docblock)
* Or read the individual method documentation for more information.
* 
* Currently supports GET, POST, HEAD, PUT, DELETE, PATCH requests.
* Other requests types is possible by using the ->call(); method.
*
* This class implements the magic method __cal(); in a way that allows you to call any curl_* function
* That has not already been implemented by this class, while omitting the curl handle.
*
* @author Allan Thue Rehhoff <http://rehhoff.me>
* @version 1.3.1
* @link https://bitbucket.org/allanrehhoff/phptools/src
*/

class WebRequest {
	private $curl, $response, $verbose, $rawHeaders;
	private $curlInfo = [];
	private $headers = [];
	private $responseHeaders = [];
	private $cookies = [];
	private $options = [];

	const GET = "GET";
	const POST = "POST";
	const HEAD = "HEAD";
	const PUT = "PUT";
	const DELETE = "DELETE";
	const PATCH = "PATCH";

	/**
	* The constructor takes a single argument, the url of the host to request.
	* @param (string) $url A fully qualified url, on which the service can be reached.
	*/
	public function __construct($url = null) {
		$this->curl = curl_init();

		$this->options = [
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_MAXREDIRS => 5,
			CURLOPT_SSL_VERIFYPEER => true,
			// CURLOPT_URL defaults to null, by assigning a potentially unmodified argument we ensure cURL behaves as it normally would
			CURLOPT_URL => $url
		];
	}

	/**
	* Do not bother about this method, you should not be calling this.
	* @return void
	*/
	public function __destruct() {
		curl_close($this->curl);
		if(is_resource($this->verbose)) {
			fclose($this->verbose);
		}
	}

	/**
	* Allows usage for any curl_* functions in PHP not implemented by this class.
	* @param (string) $function - cURL function to call without, curl_ part must be ommited.
	* @param (array) $params - Array of arguments to pass to $function.
	* @return object
	* @link http://php.net/manual/en/ref.curl.php
	*/
	public function __call($function, $params) {
		if(function_exists("curl_".$function)) {
			array_unshift($params, $this->curl);
			call_user_func_array("curl_".$function, $params);
		} else {
			throw new Exception($function." is not a valid cURL function.");
		}
		return $this;
	}

	/**
	* The primary function of this class, performs the actual call to a specified service. Parsing the headers afterwards.
	* @param (string) $method HTTP method to use for this request.
	* @param (mixed) $data The full data to "post" with this request
	* @param (int) $timeout Seconds this request shall last before it times out.
	* @return object
	*/
	public function call($method = false, $data = false, $timeout = 60) {
		// Make sure data are sent in a correct format.
		if($method === self::GET) {
			$getRequestParams = ($data !== false) ? '?'.http_build_query($data, '', '&') : ''; //($data !=== false) ? '?'.http_build_query($data) : '';
			$this->setUrl($this->getUrl().$getRequestParams);
			$this->setOption(CURLOPT_HTTPGET, true);
		} elseif($method !== false) {
			$this->setOption(CURLOPT_CUSTOMREQUEST, $method);
			if($data !== false) {
				$this->setOption(CURLOPT_POSTFIELDS, $data);
			}
		}

		$headerHandle = fopen("php://temp", "rw+");

		$this->setOption(CURLOPT_HTTPHEADER, $this->headers);
		$this->setOption(CURLOPT_TIMEOUT, $timeout);
		$this->setOption(CURLOPT_WRITEHEADER, $headerHandle);

		// Send cookies associated with this request
		if(!empty($this->cookies)) {
			$cookieString = '';
			$iterations = 0;
			$numCookiesSet = count($this->cookies);

			foreach($this->cookies as $cookie) {
				$iterations++;
				$cookieString .= $cookie->name.'='.$cookie->value;
				if($iterations < $numCookiesSet) $cookieString .= "; ";
			}

			$this->setOption(CURLOPT_COOKIE, $cookieString); 
		}
		
		// Finally perform the request
		curl_setopt_array($this->curl, $this->options);
		$this->response = curl_exec($this->curl);
		$this->curlInfo = curl_getinfo($this->curl);

		if($this->response === false) {
			throw new Exception(curl_errno($this->curl).": ".curl_error($this->curl), curl_errno($this->curl));
		}

		// And parse the headers for a client to use.
		rewind($headerHandle); 
		$this->rawHeaders = rtrim(stream_get_contents($headerHandle), "\r\n");
		$headersArray = array_filter(explode("\r\n", $this->rawHeaders));
		fclose($headerHandle);

		$this->responseHeaders["Set-Cookie"] = [];
		foreach($headersArray as $i => $line) {
			if ($i === 0) {
				$this->responseHeaders['Http-Code'] = $line;
			} else {
				$header = explode(': ', $line);

				// Makes understanding cookies recieved by the remote somewhat easier for the next developer.
				if($header[0] == "Set-Cookie") {
					preg_match("/.+?(?=\=)/", $header[1], $cookieName);
					$this->responseHeaders["Set-Cookie"][$cookieName[0]] = $header[1];
				} elseif (isset($header[1])) {
					$this->responseHeaders[$header[0]] = trim($header[1]);
				} else {
					$this->responseHeaders[] = trim($header[0]);
				}
			}
		}

		if(is_resource($this->verbose)) {
			rewind($this->verbose); //@todo: Why do I need this, I'm still wondering...
			$verboseContent = stream_get_contents($this->verbose);

			$this->rawHeaders .= $verboseContent;
			$this->responseHeaders["verbosity"] = explode("\n", $verboseContent);
		}
		
		return $this;
	}

	/**
	* Perform the request through HTTP GET
	* @param (mixed) $data
	* 	Parameters to send with this request, see the call method for more information on this parameter.
	*	Naturally you should not find a need for this parameter, but it is implemented just in case the server masquarades.
	*
	* @param (int) $timeout - Seconds this request shall last before it times out.
	* @return object
	*/
	public function get($data = false, $timeout = 60) {
		return $this->call(self::GET, $data, $timeout);
	}

	/**
	* Perform the request through HTTP POST
	* @param (mixed) $data Postfields to send with this request, see the call method for more information on this parameter
	* @param (int) $timeout Seconds this request shall last before it times out.
	* @return object
	*/
	public function post($data = false, $timeout = 60) {
		return $this->call(self::POST, $data, $timeout);
	}

	/**
	* Obtain metainformation about the request without transferring the entire message-body
	*A HEAD request does not accept post data, so the $data parameter is not available here.
	* 
	* @param (int) $timeout Seconds this request shall last before it times out.
	* @return object
	*/
	public function head($timeout = 60) {
		return $this->call(self::HEAD, false, $timeout);
	}

	/**
	* Put data through HTTP PUT.
	* @param (mixed) $data Data to send through this request, see the call method for more information on this parameter.
	* @param (int) $timeout Seconds this request shall last before it times out.
	* @return object
	*/
	public function put($data = false, $timeout = 60) {
		return $this->call(self::PUT, $data, $timeout);
	}

	/**
	* Requests that the origin server delete the resource identified by the Request-URI.
	* @param (mixed) $data 
	*	When using this parameter you should consider signaling the pressence of a message body
	*	By providing a Content-Length or Transfer-Encoding header.
	*
	* @param (int) $timeout - Seconds this request shall last before it times out.
	*/
	public function delete($data = false, $timeout = 60) {
		return $this->call(self::DELETE, $data, $timeout);
	}

	/**
	* Patch those data to the service.
	* @param (mixed) $data - Data to send with this requst.
	* @param (int) $timeout Seconds this request shall last before it times out.
	*/
	public function patch($data = false, $timeout = 60) {
		return $this->call(self::PATCH, $data, $timeout);
	}

	/**
	* Get cURL information regarding this request.
	* If index is given, returns its value, NULL if index is undefined.
	* Otherwise, returns an associative array of all available values.
	*
	* @param (string) $opt An index from curl_getinfo() returned array.
	* @return mixed
	* @see http://php.net/manual/en/function.curl-getinfo.php
	* @throws Exception
	*/
	public function getInfo($opt = false) {
		if(empty($this->curlInfo)) {
			throw new Exception("A cURL session has yet to be performed.");
		}

		if($opt !== false) {
			return isset($this->curlInfo[$opt]) ? $this->curlInfo[$opt] : null;
		}

		return $this->curlInfo;
	}

	/**
	* Gives the raw response returned by remote server.
	* @since 1.2
	* @return string
	*/
	public function getRawResponse() {
		return  $this->rawHeaders."\r\n".$this->response;
	}

	/**
	* Returns parsed header values.
	* If header is given returns that headers value.
	* Otherwise all response headers is returned.
	* 
	* @param (string) $header Name of the header for which to get the value
	* @return mixed
	*/
	public function getResponseHeaders($header = false) {
		if($header !== false) {
			return isset($this->responseHeaders[$header]) ? $this->responseHeaders[$header] : null;
		}

		return $this->responseHeaders;
	}

	/**
	* Alias method for WebRequest::getResponseHeaders(); Implemented because of the readability drove me nuts.
	* @param (string) $header Name of the header for which to get the value
	* @return mixed
	*/
	public function getResponseHeader($header = false) {
		return $this->getResponseHeaders($header);
	}

	/**
	* Get the request response text without the headers.
	* @return string
	*/
	public function getResponse() {
		if($this->response === null) {
			throw new Exception("Perform the request before accessing response text.");
		}
		return $this->response;
	}

	/**
	* Provide an additional header for this request.
	* @param (string) $header The header to send with this request.
	* @return object
	*/
	public function setHeader($header) {
		$this->headers[] = $header;
		return $this;
	}

	/**
	* Send a cookie with this request.
	* @param (string) $name name of the cookie
	* @param (string) $value value of the cookie
	* @return object
	*/
	public function setCookie($name, $value) {
		$this->cookies[$name] = (object) [
			"name" => $name,
			"value" => $value
		];
		return $this;
	}

	/**
	* Get cookies set by the remote server for the performed request.
	* @since 1.2
	* @param (string) $cookie Name of the cookie for which to retrieve details, null if it doesn't exist, ommit to get all cookies.
	* @return array
	*/
	public function getCookie($cookie = false) {
		if($cookie !== false) {
			return isset($this->responseHeaders["Set-Cookie"][$cookie]) ? $this->responseHeaders["Set-Cookie"][$cookie] : null;
		}
		return $this->responseHeaders["Set-Cookie"];
	}

	/**
	* Alias for WebRequest::getCookie(); implemented because readability when getching all cookies drove me nuts.
	* @return array
	* @since 1.2
	*/
	public function getCookies() {
		return $this->getCookie();
	}

	/**
	* Manually set a cURL option for this request.
	* @param (int) $option The CURLOPT_XXX option to set.
	* @param (mixed) Value for the option
	* @return  object
	* @see http://php.net/curl_setopt
	*/
	public function setOption($option, $value) {
		$this->options[$option] = $value;
		//curl_setopt($this->curl, $option, $value);
		return $this;
	}

	/**
	* Retrieve the current value of a given cURL option
	* @param (int) $option CURLOPT_* value to retrieve
	* @return mixed
	* @since 1.3
	*/
	public function getOption($option) {
		return $this->options[$option];
	}

	/**
	* A string to use as authorization for this request.
	* @param (string) $usrpwd
	* 	A combination of username and password
	*	Typically in the format username:password
	* @param (int) $authType The HTTP authentication method(s) to use
	* @return object
	*/
	public function authorize($usrpwd, $authType = CURLAUTH_BASIC) {
:		$this->setOption(CURLOPT_HTTPAUTH, $authType);
		$this->setOption(CURLOPT_USERPWD, $usrpwd);
		return $this;
	}

	/**
	* Enable CURL verbosity, captures and pushes the output to the response headers.
	* @return object
	*/
	public function verbose() {
		$this->verbose = fopen('php://temp', 'rw+');
		$this->setOption(CURLOPT_VERBOSE, true);
		$this->setOption(CURLOPT_STDERR, $this->verbose);

		return $this;
	}

	/**
	* Sets destination url, to which this request will be sent.
	* @param $value a fully qualified url
	* @return object
	*/
	public function setUrl($value) {
		//$this->url = $value;
		$this->setOption(CURLOPT_URL, $value);
		return $this;
	}

	/**
	* Get the URL to be requested.
	* @return string
	* @since 1.1
	*/
	public function getUrl() {
		return $this->getOption(CURLOPT_URL);
	}
}