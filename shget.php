#!/usr/bin/php
<?php
	/*
	* DESCRIPTION:
	* After installing this script you should be able to download, extract and removea single file from a given remote url.
	* The given url will automatically be resolved if it's the given url is redirecting, (up to 10 redirects allowed)
	* Current supports the following archives: tbz, bz2, zip, gz, tar
	* 
	* INSTALL:
	* Add the following line to your .bash_profile, .bashrc, Or any other suitable place...
	* alias shget='sh /home/xxxxx/tools/shget.php'
	*
	* Alternatively, place the file in /usr/local/bin
	* and run it by simply typing shget.php in terminal (you may need to restart terminal)
	* Rename file to shget (no extension) if you just want to type shget in terminal.
	*
	* The PHP script needs to be executable by the user who is running it.
	* If the script is at /home/me/shget2.php and you only want yourself to be able to read, write and execute it then chmod like so:
	* chmod 0700 /home/me/shget2.php
	*
	* TROUBLESHOOTING:
	* Q:/usr/bin/shget - Permission denied
	* A: Make the file executeable, and check if the user executing has the right permissions to access the file.
	*
	* Q: Where do I find .bash_profile and .-bashrc files?
	* A: Those files are usually located in home directory 'cd ~/'
	*
	* Q: /usr/bin/php bad intepreter: no such file or directory.
	* A: Edit the shebang above to match your php installation directory.
	*    Also make sure PHP5 cli packages are installed.. 
	*    sudo apt-get update
	*    sudo apt-get install php5-cli
	*/
	function confirm($question, $persistant = true) {
		while(true) {
			echo $question.' [Y/N] : ';
			$answer = fgets(fopen("php://stdin","r"));

			if($persistant) {
				if(preg_match('/^[Yy]/', $answer)) {
					return true;
				} elseif(preg_match('/^[Nn]/', $answer)) {
					return false;
				} else {
					echo "Not a valid answer \r\n";
				}
			} else {
				return preg_match('/^[Yy]/', $answer);
			}
		}
	}
	/*function color($text, $color=null) {
		$colors = array(
			'BLACK'       => "0;30m",
			'GRAY'        => "1;30m",
			'LIGHTGRAY'   => "0;37m",
			'BLUE'        => "0;34m",
			'LIGHTBLUE'   => "1;34m",
			'GREEN'       => "0;32m",
			'LIGHTGREEN'  => "1;32m",
			'CYAN'        => "0;36m",
			'LIGHTCYAN'   => "1;36m",
			'RED'         => "0;31m",
			'LIGHTRED'    => "1;31m",
			'PURPLE'      => "0;35m",
			'LIGHTPURPLE' => "1;35m",
			'BROWN'       => "0;33m",
			'YELLOW'      => "1;33m",
			'WHITE'       => "1;37m",
	 
			// background colors
			'SUCCESS'     => "42m",
			'FAILURE'     => "41m",
			'WARNING'     => "43;30m", // black yellow 
			'NOTE'        => "44m",
			'INVERT'      => "47;30m", // black on white
		);
	 
		if ($color != null && isset($colors[strtoupper($color)])) {
			$output = $colors[strtoupper($color)];
			$text   = chr(27) . '[' . $output . $text . chr(27) . '[0m';
		}
		return $text;
	} */
	
	function get_remote_info($url){
		$options = array(
				 CURLOPT_RETURNTRANSFER => true,
				 CURLOPT_HEADER		    => false,	
				 CURLOPT_FOLLOWLOCATION => true,
				 CURLOPT_ENCODING	    => "",
				 CURLOPT_USERAGENT	    => "spider", 
				 CURLOPT_AUTOREFERER	=> true,
				 CURLOPT_FAILONERROR    => true,
				 CURLOPT_CONNECTTIMEOUT => 10,	
				 CURLOPT_TIMEOUT		=> 10,	
				 CURLOPT_MAXREDIRS	    => 10,
				 CURLOPT_NOBODY         => true
		);

		$ch	  = curl_init($url);
		curl_setopt_array($ch, $options);
		curl_exec($ch);
		$url = curl_getinfo($ch);
		curl_close($ch);
		return $url;
	}
	
	if(count($argv) <= 1) {
		echo "UNRECOVERABLE ERROR: No additional paramters was provided. \r\n";
		exit;
	}

	if(!filter_var(end($argv), FILTER_VALIDATE_URL)) {
		echo "UNRECOVERABLE ERROR: You must pass a valid remote url as the last parameter.";
		exit;
	}
	
	$initial_directory = getcwd();
	echo "Initial directory is ".$initial_directory."\r\n";

	/*$accepted_mime_types = array(
		'application/zip',
		'application/x-gzip',
		'application/bzip2',
		'application/x-bz2',
		'application/x-bzip',
		'application/octet-stream'
	);*/
	
	$remote_file = (object) get_remote_info(end($argv));
	$basename = basename($remote_file->url);
	$extension = end(explode('.', $basename));
	$effective_url = $remote_file->url;
	$expected_http_code = 200;
	$got_unexpected_http_code = false;
	
	/*if(!in_array($remote_file->content_type, $accepted_mime_types)) {
		echo "Mime type ".$remote_file->content_type." is not supported\r\n";
		echo "Supported mime types are: \r\n".implode(", ", $accepted_mime_types)."\r\n";
		exit;
	} */
		
	if($remote_file->redirect_count > 0) {
		echo "The given url was redirected " . $remote_file->redirect_count . ' times. '."\r\n";
		echo "Effective url is: " . $effective_url . "\r\n";
	}

	if($remote_file->http_code != $expected_http_code) {
		$got_unexpected_http_code = true;
		echo "WARNING: Recieved an unexpected HTTP CODE expoected ".$expected_http_code.", got ".$remote_file->http_code."\r\n";
		if(!confirm('Do you want to continue?')) {
			echo "Aborted...\r\n";
			exit;
		}
		echo "The file will not be extracted..\r\n";
	}
	
	$file_download = exec("wget ".escapeshellarg($effective_url));
	
	if(!$got_unexpected_http_code) {
		echo "Extracting: ".$basename."\r\n";
		$available_commands = array(
			'gz' => 'tar -zxvf',
			'tbz' => 'tar -xjf',
			'bz2' => 'bzip2 -dk',
			'zip' => 'unzip'
		);
		
		if(!isset($available_commands[$extension])) {
			echo "UNRECOVERABLE ERROR: Extracting this type of archive is not supported, please continue manually.\r\n";
			exit;
		}
		
		$command = $available_commands[$extension];
		$executed_command = $command . ' ' . escapeshellarg($basename);
		exec($executed_command);
		echo "Removing archive...\r\n";
		unlink($basename);
	}
	echo "Done...\r\n";
	exit;
?>